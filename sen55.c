/*
  Sensirion SEN55 driver, which outputs JSON
  Author: xnavra50
*/
#include <math.h>  // NAN
#include <stdio.h> // printf

#include "sen5x_i2c.h"
#include "sensirion_common.h"
#include "sensirion_i2c_hal.h"

int main(void)
{
  int16_t error = 0;

  sensirion_i2c_hal_init();

  error = sen5x_device_reset();
  if (error)
  {
    fprintf(stderr, "Error executing sen5x_device_reset(): %i\n", error);
  }

  uint8_t firmware_major;
  uint8_t firmware_minor;
  bool firmware_debug;
  uint8_t hardware_major;
  uint8_t hardware_minor;
  uint8_t protocol_major;
  uint8_t protocol_minor;
  error = sen5x_get_version(&firmware_major, &firmware_minor, &firmware_debug,
                            &hardware_major, &hardware_minor, &protocol_major,
                            &protocol_minor);
  if (error)
  {
    fprintf(stderr, "Error executing sen5x_get_version(): %i\n", error);
  }

  // set a temperature offset in degrees celsius
  // Note: supported by SEN54 and SEN55 sensors
  // By default, the temperature and humidity outputs from the sensor
  // are compensated for the modules self-heating. If the module is
  // designed into a device, the temperature compensation might need
  // to be adapted to incorporate the change in thermal coupling and
  // self-heating of other device components.
  //
  // A guide to achieve optimal performance, including references
  // to mechanical design-in examples can be found in the app note
  // “SEN5x – Temperature Compensation Instruction” at www.sensirion.com.
  // Please refer to those application notes for further information
  // on the advanced compensation settings used in
  // `sen5x_set_temperature_offset_parameters`,
  // `sen5x_set_warm_start_parameter` and `sen5x_set_rht_acceleration_mode`.
  //
  // Adjust temp_offset to account for additional temperature offsets
  // exceeding the SEN module's self heating.
  float temp_offset = 0.0f;
  error = sen5x_set_temperature_offset_simple(temp_offset);
  if (error)
  {
    fprintf(stderr, "Error executing sen5x_set_temperature_offset_simple(): %i\n",
            error);
  }

  // Start Measurement
  error = sen5x_start_measurement();

  if (error)
  {
    fprintf(stderr, "Error executing sen5x_start_measurement(): %i\n", error);
  }

  while (true)
  {
    // Read Measurement
    sensirion_i2c_hal_sleep_usec(15000000); // 15 seconds

    float mass_concentration_pm1p0;
    float mass_concentration_pm2p5;
    float mass_concentration_pm4p0;
    float mass_concentration_pm10p0;
    float ambient_humidity;
    float ambient_temperature;
    float voc_index;
    float nox_index;

    error = sen5x_read_measured_values(
        &mass_concentration_pm1p0, &mass_concentration_pm2p5,
        &mass_concentration_pm4p0, &mass_concentration_pm10p0,
        &ambient_humidity, &ambient_temperature, &voc_index, &nox_index);
    if (error)
    {
      fprintf(stderr, "Error executing sen5x_read_measured_values(): %i\n", error);
    }
    else
    {
      // first write to a temporary file, then rename it to
      // avoid reading a partially written file
      // mv should be atomic on Linux
      FILE *file = fopen("sen55.json.tmp", "w");
      if (file == NULL)
      {
        fprintf(stderr, "Unable to open the file.\n");
        return 1;
      }

      fprintf(file, "{\n");

      // Mass concentration pm1p0: %.1f µg/m³
      fprintf(file, "    \"pm1p0\": %.1f,\n",
              mass_concentration_pm1p0);
      fprintf(file, "    \"pm2p5\": %.1f,\n",
              mass_concentration_pm2p5);
      fprintf(file, "    \"pm4p0\": %.1f,\n",
              mass_concentration_pm4p0);
      fprintf(file, "    \"pm10p0\": %.1f,\n",
              mass_concentration_pm10p0);

      if (isnan(ambient_humidity))
      {
        fprintf(file, "    \"humidity\": null,\n");
      }
      else
      {
        fprintf(file, "    \"humidity\": %.1f,\n", ambient_humidity);
      }

      if (isnan(ambient_temperature))
      {
        fprintf(file, "    \"temperature\": null,\n");
      }
      else
      {
        fprintf(file, "    \"temperature\": %.1f,\n", ambient_temperature);
      }

      if (isnan(voc_index))
      {
        fprintf(file, "    \"voc\": null,\n");
      }
      else
      {
        fprintf(file, "    \"voc\": %.1f,\n", voc_index);
      }

      if (isnan(nox_index))
      {
        fprintf(file, "    \"nox\": null\n");
      }
      else
      {
        fprintf(file, "    \"nox\": %.1f\n", nox_index);
      }

      fprintf(file, "}\n");

      fclose(file);

      if (rename("sen55.json.tmp", "sen55.json") != 0)
      {
        fprintf(stderr, "Unable to rename file\n");
      }
    }
  }
}
